import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {TeamMembers} from '../_models/team-members';

@Injectable()
export class TeamMembersService {
  private apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) {}

  public getTeamMembers(): Observable<TeamMembers> {
    return this.httpClient.get<TeamMembers>(this.apiUrl);
  }
}
