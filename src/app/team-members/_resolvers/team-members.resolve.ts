import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {TeamMembersService} from '../_services/team-members.service';
import {TeamMemberData, TeamMembers} from '../_models/team-members';
import {map} from 'rxjs/operators';

@Injectable()
export class TeamMembersResolve implements Resolve<TeamMemberData[]> {

  constructor(private teamMembersService: TeamMembersService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TeamMemberData[]> {
    return this.teamMembersService.getTeamMembers().pipe(
      map((teamMembers: TeamMembers) => teamMembers.data)
    );
  }
}
