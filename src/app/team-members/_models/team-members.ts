export interface TeamMembers {
  data: TeamMemberData[];
}

export interface TeamMemberData {
  type: string;
  id: string;
  attributes: Attributes;
}

export interface Attributes {
  title: string;
  memberCards: MemberCards;
}

export interface MemberCards {
  first: First;
  second: Second;
  third: Third;
}

export interface First {
  imageUrl: ImageUrl;
  block: Block;
}

export interface Second {
  imageUrl: ImageUrl;
  block: Block;
}

export interface Third {
  imageUrl: ImageUrl;
  block: Block;
}

export interface ImageUrl {
  w200: string;
  w400: string;
  w1080: string;
  w1920: string;
  w2560: string;
}

export interface Block {
  title: string;
  description: string;
  link: string;
  text: string;
}


