import {ChangeDetectionStrategy, Component, Input, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-single-member-title',
  templateUrl: './single-member-title.component.html',
  styleUrls: ['./single-member-title.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class SingleMemberTitleComponent {
  @Input() public title: string;

  constructor() {}

}
