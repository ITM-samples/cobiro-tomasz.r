import {ChangeDetectionStrategy, Component, Input, ViewEncapsulation} from '@angular/core';
import {Block, ImageUrl} from '../../_models/team-members';

@Component({
  selector: 'app-single-member-card',
  templateUrl: './single-member-card.component.html',
  styleUrls: ['./single-member-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class SingleMemberCardComponent {
  @Input() public imageUrl: ImageUrl;
  @Input() public data: Block;

  constructor() { }

}
