import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {TeamMembersRoutingModule} from './team-members-routing.module';
import {TeamMembersComponent} from './_containers';
import {TeamMembersService} from './_services';
import {TeamMembersResolve} from './_resolvers';
import {SingleMemberCardComponent, SingleMemberTitleComponent} from './_components';

@NgModule({
  declarations: [
    TeamMembersComponent,
    SingleMemberCardComponent,
    SingleMemberTitleComponent
  ],
  imports: [
    CommonModule,
    TeamMembersRoutingModule
  ],
  providers: [
    TeamMembersService,
    TeamMembersResolve,
  ]
})
export class TeamMembersModule {}
