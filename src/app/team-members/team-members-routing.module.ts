import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TeamMembersComponent} from './_containers/team-members/team-members.component';
import {TeamMembersResolve} from './_resolvers/team-members.resolve';

const routes: Routes = [
  {
    path: '',
    component: TeamMembersComponent,
    resolve: {
      teamMembers: TeamMembersResolve
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamMembersRoutingModule {}
