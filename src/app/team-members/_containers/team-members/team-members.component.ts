import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TeamMemberData} from '../../_models/team-members';

@Component({
  selector: 'app-team-members',
  templateUrl: './team-members.component.html',
  styleUrls: ['./team-members.component.scss']
})
export class TeamMembersComponent implements OnInit {
  public teamMembers: TeamMemberData[] = this.activatedRoute.snapshot.data.teamMembers;

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {}

}
