import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'team-members',
    pathMatch: 'full',
  },
  {
    path: '',
    children: [
      {
        path: 'team-members',
        loadChildren: () => import('./team-members/team-members.module').then(m => m.TeamMembersModule),
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'team-members',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
